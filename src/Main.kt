/**
 * deadlock: 2 resources, 2 threads:
 * thread1 waits for res1, which is
 * assigned to thread2. thread2 waits
 * for res2 which is assigned to thread1
 * they block each other
 * */
    fun main(args: Array<String>){
        val res1 = Any() // 'Object' class from java
        val res2 = Any()
        val thread1 = object: Thread(){
            override fun run(){
                synchronized(res1){
                    // thread1 assigns res1 to itself
                    println("thread1 assigns res1 to itself...")
                    try {
                       Thread.sleep(100)
                        // 0.1 sec delay for thread2 to assign res2
                    }catch (e: Throwable){
                        println(e.stackTrace)
                    }
                    synchronized(res2){
                        // thread1 would try to assign res2 to itself, but it's going to suck
                        // because while delay thread2 would already assign res2 to itself
                        println("thread1 assigns res2 to itself...")
                    }
                }
            }
        }
        val thread2 = object: Thread(){
            override fun run(){
                synchronized(res2){
                    println("thread2 assigns res2 to itself...")
                    // this may happen during the delay 0.1 sec above
                    try {
                        // 0.1 sec delay for thread1 to assign res1 to itself
                        // independently on whether it's started firstly or secondly
                        Thread.sleep(100)
                    }catch (e:Throwable){
                        println(e.stackTrace)
                    }
                    synchronized(res1){
                        // thread2 would try to assign res1 to itself
                        // but it would suck, because it's already assigned by thread1
                        // during the 0.1 sec delay above
                        println("thread1 assigns res1 to itself...")
                    }
                }
            }
        }
        thread1.start()
        thread2.start()
        // they lock each other and so the message below will never be shown
        if(!thread1.isAlive() || !thread2.isAlive()) println("deadlocked message, b*tch!!1!11")
    }